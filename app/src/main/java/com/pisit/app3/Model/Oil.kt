package com.pisit.app3.Model

data class Oil (val name: String, val price: Double)